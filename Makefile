LDFLAGS=-g
CXX=g++

simple_linked_list: SimpleLinkedList.o
					$(CXX) $(LDFLAGS) -o simple_linked_list SimpleLinkedList.o
clean:
					rm *.o
