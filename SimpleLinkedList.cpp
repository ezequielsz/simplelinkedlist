#include "SimpleLinkedList.h"
#include <cstddef>
#include <cstdlib>
#include <ctime>
#include <iostream>

using namespace std;

SimpleLinkedList::SimpleLinkedList() {
	head = NULL;
	last = NULL;
	size = 0;
}

SimpleLinkedList::~SimpleLinkedList() {
	while(head != NULL) {
		Node *next = static_cast<Node *>(head->second);
		delete head;
		head = next;
	}
}

int SimpleLinkedList::Size() {
	return this->size;
}

void SimpleLinkedList::PushBack(int value) {
	Node *newNode = new Node(value, NULL);

	if (head == NULL) {
		head = newNode;
		last = head;
	} else {
		last->second = newNode;
		last = newNode;
	}

	this->size++;
}

void SimpleLinkedList::PushFront(int value) {
	Node *newNode = new Node(value, head);
	head = newNode;

	if (last == NULL)
		last = head;

	this->size++;
}

int SimpleLinkedList::PopFront() {
	if (size > 0) {
		Node *tmp = static_cast<Node *>(head->second);
		int value = head->first;
		delete head;
		head = tmp;
		this->size--;
		return value;
	}
	return 0;
}


enum Op {PUSH_BACK, PUSH_FRONT};

int main()
{
	SimpleLinkedList *list = new SimpleLinkedList();

	for (int i = 0; i < 10; i++) {
		int n = rand() % 100;
		int op = rand() % 3;
		switch(op) {
			case PUSH_BACK:
				list->PushBack(n);
				cout << "PushBack(" << n << ")" << endl;
				break;
			case PUSH_FRONT:
				list->PushFront(n);
				cout << "PushFront(" << n << ")" << endl;
				break;
			default:
				break;
		}
	}

	for (int i = 0; i < 10; i++) {
		cout << list->PopFront() << endl;
	}

	delete list;

	return 0;
}
