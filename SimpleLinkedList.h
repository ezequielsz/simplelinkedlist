#ifndef SIMPLELINKEDLIST_H
#define SIMPLELINKEDLIST_H

#include <utility>

using namespace std;

typedef pair<int, void *> Node;

class SimpleLinkedList {
public:
	SimpleLinkedList();
	~SimpleLinkedList();
	int Size();
	void PushBack(int value);
	void PushFront(int value);
	int PopFront();

private:
	int size;
	Node *head;
	Node *last;
};

#endif
